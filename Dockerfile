FROM hub.eole.education/proxyhub/alpine:latest as BUILD
RUN apk add --no-cache git npm
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digitools/digitools-sources.git && \
    cd digitools-sources && \
    git checkout $(cat ../app-version) && \
    cp -a /tmp/digitools-sources /tmp/src
FROM hub.eole.education/proxyhub/alpine:latest
RUN apk add --no-cache apache2 php83-apache2 php83-zip php83-pdo_pgsql php83-pdo_sqlite php83-pecl-redis
RUN rm /var/www/localhost/htdocs/index.html
COPY --from=BUILD /tmp/digitools-sources/ /var/www/localhost/htdocs/
COPY 000-default.conf /etc/apache2/conf.d/000-default.conf
RUN sed -i "s/^ServerTokens .*$/ServerTokens Prod/" /etc/apache2/httpd.conf
RUN sed -i "s/^ServerSignature .*$/ServerSignature Off/" /etc/apache2/httpd.conf
RUN chown -R apache: /var/www/localhost/htdocs
EXPOSE "80"
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND", "-f", "/etc/apache2/httpd.conf"]
